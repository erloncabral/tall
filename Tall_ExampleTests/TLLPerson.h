//
//  LLPerson.h
//  Tall
//
//  Created by Erlon Olinda Cabral on 7/29/13.
//  Copyright (c) 2013 Erlon Olinda Cabral. All rights reserved.
//

#import "TLLModel.h"

@interface TLLPerson : TLLModel

@property (copy, nonatomic) NSString *firstName;
@property (copy, nonatomic) NSString *lastName;
@property (assign, nonatomic, getter = isDead) BOOL dead;
@property (assign, nonatomic) double weight;
@property (assign, nonatomic) double height;
@property (strong, nonatomic) NSDate *birthday;

@end
