//
//  TLLColumnTests.m
//  Tall_Example
//
//  Created by Erlon Olinda Cabral on 04/08/13.
//  Copyright (c) 2013 Erlon Cabral. All rights reserved.
//

#import "TLLColumnTests.h"

#import "TLLModel.h"
#import "TLLColumn.h"
#import "FMResultSet.h"

@implementation TLLColumnTests

- (void)test_object_creation
{
    TLLColumn *primaryKeyColumn = [[TLLColumn alloc] initWithPrimaryKeyName:@"id" propertyName:@"primaryKey"];
    STAssertTrue([primaryKeyColumn.name isEqualToString:@"id"], @"");
    STAssertTrue([primaryKeyColumn.propertyName isEqualToString:@"primaryKey"], @"");
    STAssertTrue(primaryKeyColumn.primaryKey, @"");
    STAssertTrue(primaryKeyColumn.type == TLLColumnTypeInt, @"");
    STAssertNotNil(primaryKeyColumn.mapper, @"");
    
    TLLColumn *customMapperColumn = [[TLLColumn alloc] initWithName:@"first_name" propertyName:@"firstName" columnType:TLLColumnTypeString mapper:^(TLLModel *model, TLLColumn *column, FMResultSet *resultSet) {
        [model setValue:[resultSet stringForColumn:column.name] forKey:column.propertyName];
    }];
                                     
    STAssertTrue([customMapperColumn.name isEqualToString:@"first_name"], @"");
    STAssertTrue([customMapperColumn.propertyName isEqualToString:@"firstName"], @"");
    STAssertTrue(!customMapperColumn.primaryKey, @"");
    STAssertTrue(customMapperColumn.type == TLLColumnTypeString, @"");
    STAssertNotNil(customMapperColumn.mapper, @"");
    
    TLLColumn *basicColumn = [[TLLColumn alloc] initWithName:@"last_name" propertyName:@"lastName" columnType:TLLColumnTypeString defaultMapper:NO];
    STAssertTrue([basicColumn.name isEqualToString:@"last_name"], @"");
    STAssertTrue([basicColumn.propertyName isEqualToString:@"lastName"], @"");
    STAssertTrue(!basicColumn.primaryKey, @"");
    STAssertTrue(basicColumn.type == TLLColumnTypeString, @"");
    STAssertNil(basicColumn.mapper, @"");
    
    TLLColumn *basicColumnWithMapper = [[TLLColumn alloc] initWithName:@"email" propertyName:@"email" columnType:TLLColumnTypeString defaultMapper:YES];
    STAssertTrue([basicColumnWithMapper.name isEqualToString:@"email"], @"");
    STAssertTrue([basicColumnWithMapper.propertyName isEqualToString:@"email"], @"");
    STAssertTrue(!basicColumnWithMapper.primaryKey, @"");
    STAssertTrue(basicColumnWithMapper.type == TLLColumnTypeString, @"");
    STAssertNotNil(basicColumnWithMapper.mapper, @"");
}

@end
