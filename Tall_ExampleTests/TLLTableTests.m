//
//  TLLTableTests.m
//  Tall_Example
//
//  Created by Erlon Olinda Cabral on 04/08/13.
//  Copyright (c) 2013 Erlon Cabral. All rights reserved.
//

#import "TLLTableTests.h"

#import "TLLTable.h"

#import "TLLSQLBuilder.h"

@implementation TLLTableTests

- (void)test_object_creation
{
    NSArray *columns = @[ [[TLLColumn alloc] initWithPrimaryKeyName:@"id" propertyName:@"primaryKey"],
                          [[TLLColumn alloc] initWithName:@"email" propertyName:@"email" columnType:TLLColumnTypeString defaultMapper:YES],
                          [[TLLColumn alloc] initWithName:@"first_name" propertyName:@"firstName" columnType:TLLColumnTypeString defaultMapper:NO] ];
    
    TLLTable *table = [[TLLTable alloc] initWithName:@"person"];
    table.columns = [columns mutableCopy];
    
    STAssertNotNil([table name], @"");
    STAssertNotNil([table primaryKeyColumn], @"");
    STAssertNotNil([table allCollumnNames], @"");
    STAssertNotNil([table allPropertyNames], @"");
}

@end
