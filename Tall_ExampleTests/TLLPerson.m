//
//  LLPerson.m
//  Tall
//
//  Created by Erlon Olinda Cabral on 7/29/13.
//  Copyright (c) 2013 Erlon Olinda Cabral. All rights reserved.
//

#import "TLLPerson.h"

@implementation TLLPerson

+ (FMDatabaseQueue *)database
{
    return [[FMDatabaseQueue alloc] initWithPath:@"/Users/erloncabral/Desktop/sample.sqlite"];
}

+ (TLLTable *)tableRepresentation
{
    TLLTable *table = [[TLLTable alloc] initWithName:@"people"];
    [table addDefaultColumnsForClassModel:[TLLPerson class] collumnNameStyle:TLLColunmNameStyleUnderscore];
    TLLColumn *primaryKey = [[TLLColumn alloc] initWithPrimaryKeyName:@"id" propertyName:@"primaryKey"];
    [table.columns insertObject:primaryKey atIndex:0];

//    LLTable *table = [[LLTable alloc] initWithName:@"people"];
//    [table addColumnWithName:@"id" fieldName:@"primaryKey" collumnType:LLCollumnTypeInteger];
//    [table addColumnWithName:@"first_name" fieldName:@"firstName" collumnType:LLCollumnTypeString];
//    [table addColumnWithName:@"last_name" fieldName:@"lastName" collumnType:LLCollumnTypeString];
//    [table addColumnWithName:@"dead" fieldName:@"dead" collumnType:LLCollumnTypeBool];
//    [table addColumnWithName:@"weight" fieldName:@"weight" collumnType:LLCollumnTypeDouble];
//    [table addColumnWithName:@"height" fieldName:@"height" collumnType:LLCollumnTypeDouble];
//    [table addColumnWithName:@"birthday" fieldName:@"birthday" collumnType:LLCollumnTypeDate];
    
    return table;
}

@end
