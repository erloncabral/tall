//
//  Tall_ExampleTests.m
//  Tall_ExampleTests
//
//  Created by Erlon Olinda Cabral on 04/08/13.
//  Copyright (c) 2013 Erlon Cabral. All rights reserved.
//

#import "Tall_ExampleTests.h"

#import "TLLPerson.h"

@implementation Tall_ExampleTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    //STFail(@"Unit tests are not implemented yet in Tall_ExampleTests");
    TLLPerson *me = [TLLPerson findById:1];
    if (!me) me = [[TLLPerson alloc] init];
    [me setPrimaryKey:1];
    [me setFirstName:@"Erlon"];
    [me setLastName:@"Cabral"];
    [me setHeight:1.76f];
    [me setWeight:65.f];
    [me setBirthday:[NSDate date]];
    [me setDead:NO];
    
    STAssertTrue([me saveRecord], @"");
    [me setLastName:@"Olinda Cabral"];
    STAssertTrue([me saveRecord], @"");
    STAssertTrue([me.lastName isEqualToString:@"Olinda Cabral"], @"");
    STAssertNotNil([TLLPerson findAll], @"");
    //STAssertTrue([[[TLLPerson findAll] lastObject] deleteRecord], @"");
}

@end
