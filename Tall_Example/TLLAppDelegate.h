//
//  TLLAppDelegate.h
//  Tall_Example
//
//  Created by Erlon Olinda Cabral on 04/08/13.
//  Copyright (c) 2013 Erlon Cabral. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TLLViewController;

@interface TLLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) TLLViewController *viewController;

@end
