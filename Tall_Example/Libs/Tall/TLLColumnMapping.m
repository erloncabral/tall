//
//  TLLColumnMapping.m
//  Tall_Example
//
//  Created by Erlon Olinda Cabral on 04/08/13.
//  Copyright (c) 2013 Erlon Cabral. All rights reserved.
//

#import "TLLColumnMapping.h"

#import "TLLModel.h"
#import "TLLColumn.h"
#import "FMResultSet.h"

#import "TLLPropertiesUtils.h"

@interface TLLColumnMapping ()

+ (TLLMapper)mapperForColumnType:(TLLColumnType)aColumnType;
+ (TLLColumnType)collumnTypeForPropertyType:(NSString *)propertyType;

@end

@implementation TLLColumnMapping { }

#pragma mark - Methods (Private)

+ (TLLMapper)mapperForColumnType:(TLLColumnType)aColumnType
{
    if (aColumnType == TLLColumnTypeBool) {
        return ^(TLLModel *model, TLLColumn *column, FMResultSet *resultSet) {
            if (![resultSet columnIsNull:column.name]) {
                [model setValue:[NSNumber numberWithBool:[resultSet boolForColumn:column.name]]
                         forKey:column.propertyName];
            }else {
                [model setValue:@NO forKey:column.propertyName];
            }
        };
    }else if (aColumnType == TLLColumnTypeInt) {
        return ^(TLLModel *model, TLLColumn *column, FMResultSet *resultSet) {
            if (![resultSet columnIsNull:column.name]) {
                [model setValue:[NSNumber numberWithInteger:[resultSet intForColumn:column.name]]
                         forKey:column.propertyName];
            }else {
                [model setValue:@0 forKey:column.propertyName];
            }
        };
    }else if (aColumnType == TLLColumnTypeDouble) {
        return ^(TLLModel *model, TLLColumn *column, FMResultSet *resultSet) {
            if (![resultSet columnIsNull:column.name]) {
                [model setValue:[NSNumber numberWithDouble:[resultSet doubleForColumn:column.name]]
                         forKey:column.propertyName];
            }else {
                [model setValue:@0.f forKey:column.propertyName];
            }
        };
    }else if (aColumnType == TLLColumnTypeDate) {
        return ^(TLLModel *model, TLLColumn *column, FMResultSet *resultSet) {
            if (![resultSet columnIsNull:column.name]) {
                [model setValue:[resultSet dateForColumn:column.name]
                         forKey:column.propertyName];
            }else {
                [model setValue:[NSNull null] forKey:column.propertyName];
            }
        };
    }else if (aColumnType == TLLColumnTypeData) {
        return ^(TLLModel *model, TLLColumn *column, FMResultSet *resultSet) {
            if (![resultSet columnIsNull:column.name]) {
                [model setValue:[resultSet dataForColumn:column.name]
                         forKey:column.propertyName];
            }else {
                [model setValue:[NSNull null] forKey:column.propertyName];
            }
        };
    }else { // LLCollumnTypeString
        return ^(TLLModel *model, TLLColumn *column, FMResultSet *resultSet) {
            if (![resultSet columnIsNull:column.name]) {
                [model setValue:[resultSet objectForColumnName:column.name]
                         forKey:column.propertyName];
            }else {
                [model setValue:[NSNull null] forKey:column.propertyName];
            }
        };
    }
}

+ (TLLColumnType)collumnTypeForPropertyType:(NSString *)propertyType
{
    if ([propertyType isEqualToString:@"c"]) {
        return TLLColumnTypeBool;
    }else if ([propertyType isEqualToString:@"d"]) {
        return TLLColumnTypeDouble;
    }else if ([propertyType isEqualToString:@"i"] ||
              [propertyType isEqualToString:@"l"] ||
              [propertyType isEqualToString:@"I"]){
        return TLLColumnTypeInt;
    }else if ([propertyType isEqualToString:@"NSDate"]) {
        return TLLColumnTypeDate;
    }else if ([propertyType isEqualToString:@"NSData"]) {
        return TLLColumnTypeData;
    }else if ([propertyType isEqualToString:@"NSString"]) {
        return TLLColumnTypeString;
    }else {
        return -1;
    }
}

#pragma mark - Methods (Public)

+ (TLLMapper)mapperForColumn:(TLLColumn *)aColumn
{
    return [[self class] mapperForColumnType:aColumn.type];
}

+ (void)addMapperOnCollumn:(TLLColumn *)aColumn forPropertyType:(NSString *)propertyType
{
    aColumn.type = [[self class] collumnTypeForPropertyType:propertyType];
    aColumn.mapper = [[self class] mapperForColumnType:aColumn.type];
}

@end
