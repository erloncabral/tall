//
//  TLLColumnMapping.h
//  Tall_Example
//
//  Created by Erlon Olinda Cabral on 04/08/13.
//  Copyright (c) 2013 Erlon Cabral. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(short, TLLColunmNameStyle) {
    TLLColunmNameStyleNone,
    TLLColunmNameStyleUnderscore
};

@class TLLModel, TLLColumn, FMResultSet;

typedef void (^TLLMapper)(TLLModel *model, TLLColumn *column, FMResultSet *resultSet);

@interface TLLColumnMapping : NSObject

+ (TLLMapper)mapperForColumn:(TLLColumn *)aColumn;
+ (void)addMapperOnCollumn:(TLLColumn *)aColumn forPropertyType:(NSString *)propertyType;

@end
