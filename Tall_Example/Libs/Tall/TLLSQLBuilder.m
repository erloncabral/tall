//
//  TLLSQLBuilder.m
//  Tall_Example
//
//  Created by Erlon Olinda Cabral on 04/08/13.
//  Copyright (c) 2013 Erlon Cabral. All rights reserved.
//

#import "TLLSQLBuilder.h"

#import "TLLPropertiesUtils.h"

@implementation TLLSQLBuilder

+ (NSString *)insertWithTable:(TLLTable *)table includePrimaryKey:(BOOL)include
{
    NSMutableArray *collumnNames = [[table allCollumnNames] mutableCopy];
    NSMutableArray *propertyNames = [[table allPropertyNames] mutableCopy];
    
    if (!include) {
        TLLColumn *primaryKeyColumn = [table primaryKeyColumn];
        [collumnNames removeObject:primaryKeyColumn.name];
        [propertyNames removeObject:primaryKeyColumn.propertyName];
    }
    
    return [NSString stringWithFormat:@"INSERT INTO %@ (%@) VALUES (%@)",
            table.name,
            [collumnNames componentsJoinedByString:@", "],
            [TLLPropertiesUtils propertiesToParams:propertyNames]];
}
 
+ (NSString *)updateWithTable:(TLLTable *)table whereColumn:(TLLColumn *)whereColumn;
{
    NSMutableString *update = [[NSMutableString alloc] initWithString:@""];
    
    for (TLLColumn *column in [table columns]) {
        if ([column isEqual:[table primaryKeyColumn]]) {
            continue;
        }
        
        if ([update isEqualToString:@""]) {
            [update appendFormat:@"UPDATE %@ SET %@ = :%@, ", table.name, column.name, column.propertyName];
        }else {
            [update appendFormat:@"%@ = :%@, ", column.name, column.propertyName];
        }
    }
    [update replaceCharactersInRange:NSMakeRange(update.length - 2, 2)
                          withString:[NSString stringWithFormat:@" WHERE %@ = :%@",
                                      whereColumn.name, whereColumn.propertyName]];
    return update;
}

+ (NSString *)deleteWithTable:(TLLTable *)table whereColumn:(TLLColumn *)whereColumn
{
    return [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = :%@",
            table.name,
            whereColumn.name,
            whereColumn.propertyName];
}

+ (NSString *)selectWithTable:(TLLTable *)table
{
    return [NSString stringWithFormat:@"SELECT %@ FROM %@",
            [[table allCollumnNames] componentsJoinedByString:@", "],
            table.name];
}

+ (NSString *)selectWithTable:(TLLTable *)table whereColumn:(TLLColumn *)whereColumn
{
    return [NSString stringWithFormat:@"SELECT %@ FROM %@ WHERE %@ = :%@ LIMIT 1",
            [[table allCollumnNames] componentsJoinedByString:@", "],
            table.name,
            whereColumn.name,
            whereColumn.propertyName];
}

@end
