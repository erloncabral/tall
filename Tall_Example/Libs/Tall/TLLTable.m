//
//  TLLTable.m
//  
//
//  Created by Erlon Olinda Cabral on 04/08/13.
//
//

#import "TLLTable.h"

#import "TLLPropertiesUtils.h"

@implementation TLLTable { }

#pragma mark - Object creation

- (id)init
{
    self = [super init];
    if (self) {
        _columns = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (id)initWithName:(NSString *)aName
{
    self = [super init];
    if (self) {
        _name = aName;
        _columns = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (id)initWithName:(NSString *)aName databaseName:(NSString *)aDatabaseName
{
    self = [super init];
    if (self) {
        _name = aName;
        _databaseName = aDatabaseName;
        _columns = [[NSMutableArray alloc] init];
    }
    
    return self;
}

#pragma mark - Methods (Public)

- (void)addDefaultColumnsForClassModel:(Class)clsModel collumnNameStyle:(TLLColunmNameStyle)style
{
    NSDictionary *props = [TLLPropertiesUtils propertyTypeDictionaryOfClass:clsModel];
    for (NSString *key in [props allKeys]) {
        NSString *collumnName = key;
        NSString *propertyType = [props valueForKey:key];
        
        if (style == TLLColunmNameStyleUnderscore) {
            collumnName = [TLLPropertiesUtils underscorePropertyName:key];
        }
        
        TLLColumn *column = [[TLLColumn alloc] initWithName:collumnName propertyName:key columnType:TLLColumnTypeInt defaultMapper:NO];
        [TLLColumnMapping addMapperOnCollumn:column forPropertyType:propertyType];
     
        [_columns addObject:column];
    }
}

- (void)addColumnWithName:(NSString *)aName propertyName:(NSString *)aPropertyName columnType:(TLLColumnType)aType andMapper:(TLLMapper)aMapper
{
    [_columns addObject:[[TLLColumn alloc] initWithName:aName propertyName:aPropertyName columnType:aType mapper:aMapper]];
}

- (void)addColumnWithName:(NSString *)aName propertyName:(NSString *)aPropertyName collumnType:(TLLColumnType)aType
{
    [_columns addObject:[[TLLColumn alloc] initWithName:aName propertyName:aPropertyName columnType:aType defaultMapper:YES]];
}

- (TLLColumn *)primaryKeyColumn
{
    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        return (((TLLColumn *)evaluatedObject).primaryKey == YES &&
                ((TLLColumn *)evaluatedObject).type == TLLColumnTypeInt);
    }];
    
    return [[_columns filteredArrayUsingPredicate:predicate] lastObject];
}

- (NSArray *)allCollumnNames
{
    NSMutableArray *names = [[NSMutableArray alloc] initWithCapacity:_columns.count];
    [_columns enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [names addObject:[(TLLColumn *)obj name]];
    }];
    
    return names;
}

- (NSArray *)allPropertyNames
{
    NSMutableArray *names = [[NSMutableArray alloc] initWithCapacity:_columns.count];
    [_columns enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [names addObject:[(TLLColumn *)obj propertyName]];
    }];
    
    return names;
}

@end
