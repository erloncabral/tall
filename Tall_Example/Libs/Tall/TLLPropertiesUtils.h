//
//  TLLPropertiesUtils
//  Tall
//
//  Created by Erlon Olinda Cabral on 8/1/13.
//  Copyright (c) 2013 Erlon Olinda Cabral. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <objc/objc.h>
#import <objc/runtime.h>

@interface TLLPropertiesUtils : NSObject

+ (NSString *)propertyTypeStringOfProperty:(objc_property_t) property;
+ (NSDictionary *)propertyTypeDictionaryOfClass:(Class)klass;
+ (NSString *)underscorePropertyName:(NSString *)property;
+ (NSString *)propertiesToParams:(NSArray *)collumns;

@end
