//
//  TLLTable.h
//  
//
//  Created by Erlon Olinda Cabral on 04/08/13.
//
//

#import <Foundation/Foundation.h>

#import "TLLColumn.h"

@class TLLColumnMapping;

@interface TLLTable : NSObject

@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *databaseName;
@property (strong, nonatomic) NSMutableArray *columns;

- (id)initWithName:(NSString *)aName;
- (id)initWithName:(NSString *)aName databaseName:(NSString *)aDatabaseName;

- (void)addDefaultColumnsForClassModel:(Class)clsModel collumnNameStyle:(TLLColunmNameStyle)style;
- (void)addColumnWithName:(NSString *)aName propertyName:(NSString *)aPropertyName columnType:(TLLColumnType)aType andMapper:(TLLMapper)aMapper;
- (void)addColumnWithName:(NSString *)aName propertyName:(NSString *)aPropertyName collumnType:(TLLColumnType)aType;

- (TLLColumn *)primaryKeyColumn;
- (NSArray *)allCollumnNames;
- (NSArray *)allPropertyNames;

@end
