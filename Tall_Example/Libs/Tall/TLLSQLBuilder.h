//
//  TLLSQLBuilder.h
//  Tall_Example
//
//  Created by Erlon Olinda Cabral on 04/08/13.
//  Copyright (c) 2013 Erlon Cabral. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TLLTable.h"

@interface TLLSQLBuilder : NSObject

+ (NSString *)insertWithTable:(TLLTable *)table includePrimaryKey:(BOOL)include;
+ (NSString *)updateWithTable:(TLLTable *)table whereColumn:(TLLColumn *)whereColumn;
+ (NSString *)deleteWithTable:(TLLTable *)table whereColumn:(TLLColumn *)whereColumn;

+ (NSString *)selectWithTable:(TLLTable *)table;
+ (NSString *)selectWithTable:(TLLTable *)table whereColumn:(TLLColumn *)whereColumn;

@end
