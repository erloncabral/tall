//
//  TLLModel.h
//  Tall_Example
//
//  Created by Erlon Olinda Cabral on 04/08/13.
//  Copyright (c) 2013 Erlon Cabral. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TLLTable.h"
#import "FMDatabaseQueue.h"

@class FMDatabaseQueue, TLLTable;

@interface TLLModel : NSObject

@property (assign, nonatomic) NSInteger primaryKey;

+ (FMDatabaseQueue *)database;
+ (TLLTable *)tableRepresentation;

+ (NSArray *)findAll;
+ (id)findById:(NSUInteger)aPrimaryKey;

- (BOOL)newRecord;
- (BOOL)saveRecord;
- (BOOL)deleteRecord;

@end
