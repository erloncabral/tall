//
//  TLLColumn.m
//  
//
//  Created by Erlon Olinda Cabral on 04/08/13.
//
//

#import "TLLColumn.h"

@implementation TLLColumn

- (id)initWithPrimaryKeyName:(NSString *)aName propertyName:(NSString *)aPropertyName
{
    self = [super init];
    if (self) {
        _name = aName;
        _propertyName = aPropertyName;
        _primaryKey = YES;
        _mapper = [TLLColumnMapping mapperForColumn:self];
    }
    
    return self;
}

- (id)initWithName:(NSString *)aName propertyName:(NSString *)aPropertyName columnType:(TLLColumnType)aType mapper:(TLLMapper)aMapper
{
    self = [super init];
    if (self) {
        _name = aName;
        _propertyName = aPropertyName;
        _type = aType;
        _mapper = aMapper;
    }
    
    return self;
}

- (id)initWithName:(NSString *)aName propertyName:(NSString *)aPropertyName columnType:(TLLColumnType)aType defaultMapper:(BOOL)useDefaultMapper
{
    self = [super init];
    if (self) {
        _name = aName;
        _propertyName = aPropertyName;
        _type = aType;
        _mapper = useDefaultMapper ? [TLLColumnMapping mapperForColumn:self] : NULL;
    }
    
    return self;
}

- (void)mapResultSetToModel:(FMResultSet *)aResultSet model:(TLLModel *)aModel
{
    if (_mapper) {
        _mapper(aModel, self, aResultSet);
    }
}

@end
