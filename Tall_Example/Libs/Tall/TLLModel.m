//
//  TLLModel.m
//  Tall_Example
//
//  Created by Erlon Olinda Cabral on 04/08/13.
//  Copyright (c) 2013 Erlon Cabral. All rights reserved.
//

#import "TLLModel.h"

#import "TLLSQLBuilder.h"

#import "FMDatabase.h"
#import "FMResultSet.h"
#import "FMDatabaseAdditions.h"

@implementation TLLModel { }

+ (FMDatabaseQueue *)database { return nil; }
+ (TLLTable *)tableRepresentation { return nil; }

+ (NSArray *)findAll
{
    TLLTable *table = [[self class] tableRepresentation];
    NSMutableArray *all = [[NSMutableArray alloc] init];
    
    [[[self class] database] inDatabase:^(FMDatabase *db) {
        FMResultSet *resultSet = [db executeQuery:[TLLSQLBuilder selectWithTable:table]];
        while ([resultSet next]) {
            TLLModel *model = [[[self class] alloc] init];
            [[table columns] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                TLLColumn *column = (TLLColumn *)obj;
                [column mapResultSetToModel:resultSet model:model];
            }];
            [all addObject:model];
        }
        [resultSet close];
    }];
    
    return all;
}

+ (id)findById:(NSUInteger)aPrimaryKey
{
    TLLTable *table = [[self class] tableRepresentation];
    TLLColumn *primaryKeyColumn = [table primaryKeyColumn];
    NSDictionary *params = @{ primaryKeyColumn.propertyName:  [NSNumber numberWithUnsignedInteger:aPrimaryKey] };
    __block TLLModel *model = nil;

    [[[self class] database] inDatabase:^(FMDatabase *db) {
        FMResultSet *resultSet = [db executeQuery:[TLLSQLBuilder selectWithTable:table whereColumn:primaryKeyColumn]
                          withParameterDictionary:params];
        if ([resultSet next]) {
            model = [[[self class] alloc] init];
            [[table columns] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                TLLColumn *column = (TLLColumn *)obj;
                [column mapResultSetToModel:resultSet model:model];
            }];
            [resultSet close];
        }
    }];
    
    return model;
}

- (NSDictionary *)dictionaryRepresentation
{
    NSArray *collumns = [[[self class] tableRepresentation] columns];
    NSMutableDictionary *result = [[NSMutableDictionary alloc] initWithCapacity:collumns.count];
    [collumns enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        TLLColumn *column = (TLLColumn *)obj;
        id value = [self valueForKey:column.propertyName] ? [self valueForKey:column.propertyName] : [NSNull null];
        [result setObject:value forKey:column.propertyName];
    }];
    
    return result;
}

- (BOOL)newRecord
{
    __block BOOL newRecord = YES;
    
    if (_primaryKey > 0) {
        TLLTable *table = [[self class] tableRepresentation];
        TLLColumn *primaryKeyColumn = [table primaryKeyColumn];
        
        NSString *sql = [NSString stringWithFormat:@"SELECT COUNT(%@) FROM %@ WHERE %@ = ?",
                         primaryKeyColumn.name,
                         table.name,
                         primaryKeyColumn.name];
        
        [[[self class] database] inDatabase:^(FMDatabase *db) {
            newRecord = [db intForQuery:sql, [NSNumber numberWithInt:_primaryKey]] <= 0;
        }];
    }
    
    return newRecord;
}

- (BOOL)saveRecord
{
    NSString *sql = @"";
    __block BOOL success = NO;
    BOOL includePrimaryKey = YES;
    TLLTable *table = [[self class] tableRepresentation];
    TLLColumn *primaryKeyColumn = [table primaryKeyColumn];
    NSMutableDictionary *params = [[self dictionaryRepresentation] mutableCopy];
    
    if (![self newRecord]) {
        sql = [TLLSQLBuilder updateWithTable:table whereColumn:primaryKeyColumn];
    }else {
        includePrimaryKey = _primaryKey > 0 ? YES : NO;
        sql = [TLLSQLBuilder insertWithTable:table includePrimaryKey:includePrimaryKey];
        if (!includePrimaryKey) {
             [params removeObjectForKey:primaryKeyColumn.propertyName];
        }
    }
    
    [[[self class] database] inTransaction:^(FMDatabase *db, BOOL *rollback) {
        success = [db executeUpdate:sql withParameterDictionary:params];
        if (success && !includePrimaryKey && _primaryKey <= 0) {
            _primaryKey = [db lastInsertRowId];
        }
        *rollback = !success;
    }];
    
    return success;
}

- (BOOL)deleteRecord
{
    __block BOOL success = NO;
    TLLTable *table = [[self class] tableRepresentation];
    TLLColumn *primaryKeyColumn = [table primaryKeyColumn];
    NSDictionary *params = @{ primaryKeyColumn.propertyName : [NSNumber numberWithInteger:self.primaryKey] };
    NSString *sql = [TLLSQLBuilder deleteWithTable:table whereColumn:primaryKeyColumn];
    
    [[[self class] database] inTransaction:^(FMDatabase *db, BOOL *rollback) {
        success = [db executeUpdate:sql withParameterDictionary:params];
        *rollback = !success;
    }];
    
    return success;
}

- (NSString *)debugDescription
{
    NSMutableString *debug = [[NSMutableString alloc] initWithFormat:@"<%@ ", NSStringFromClass([self class])];
    NSDictionary *dic = [self dictionaryRepresentation];
    NSArray *keys = [dic.allKeys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [keys enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [debug appendFormat:@"%@: %@, ", obj, [dic valueForKey:obj]];
    }];
    [debug replaceCharactersInRange:NSMakeRange(debug.length - 2, 2) withString:@">"];
    
    return debug;
}

@end
