//
//  TLLColumn.h
//  
//
//  Created by Erlon Olinda Cabral on 04/08/13.
//
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(short, TLLColumnType) {
    TLLColumnTypeInt,
    TLLColumnTypeBool,
    TLLColumnTypeDouble,
    TLLColumnTypeData,
    TLLColumnTypeDate,
    TLLColumnTypeString
};

#import "TLLColumnMapping.h"

@interface TLLColumn : NSObject

@property (copy, readonly, nonatomic) NSString *name;
@property (copy, readonly, nonatomic) NSString *propertyName;
@property (assign, readonly, nonatomic, getter = isPrimaryKey) BOOL primaryKey;
@property (assign, nonatomic) TLLColumnType type;
@property (copy, nonatomic) TLLMapper mapper;

- (id)initWithPrimaryKeyName:(NSString *)aName propertyName:(NSString *)aPropertyName;
- (id)initWithName:(NSString *)aName propertyName:(NSString *)aPropertyName columnType:(TLLColumnType)aType mapper:(TLLMapper)aMapper;
- (id)initWithName:(NSString *)aName propertyName:(NSString *)aPropertyName columnType:(TLLColumnType)aType defaultMapper:(BOOL)useDefaultMapper;

- (void)mapResultSetToModel:(FMResultSet *)aResultSet model:(TLLModel *)aModel;

@end
